#!/usr/bin/env bash
set -euo pipefail

if [[ ${BASH_VERSINFO[0]} -lt 5 ]]
then
  echo "warning: You are running Bash version $BASH_VERSION but this script expects at least version 5.0" >&2
fi

function show_usage()
{
  cat << USAGE
Convert notebooks to reveal.js slides and publish them in the public directory.

Usage:

  ${0##*/} [-c]

Options

  -h
    Show this help message and exit.
USAGE
  exit "$1"
}

symlink=false
while getopts 'hs' opt
do
  case "$opt" in
    s) symlink=true ;;
    h) show_usage 0 ;;
    *) show_usage 1 ;;
  esac
done

SELF=$(readlink -f "${BASH_SOURCE[0]}")
DIR=${SELF%/*/*}
cd -- "$DIR"

# Prepare the public directory.
public_dir=$DIR/public
resource_dirs=("$DIR"/{css,img,src})
mkdir -p "$public_dir"
pushd -- "$public_dir" > /dev/null
  if $symlink
  then
    for resource_dir in "${resource_dirs[@]}"
    do
      name=${resource_dir##*/}
      [[ -d $name ]] && rm -fr "$name"
      ln -sft . "$resource_dir"
    done
  else
    find . -type l -exec rm '{}' \+
    cp -LRvut . "${resource_dirs[@]}"
  fi
popd > /dev/null

# Create a directory for temporary files.
tmp_dir=$(mktemp -d)
trap "rm -fr ${tmp_dir@Q}" EXIT
tmp_path=$tmp_dir/tmp.html

# Index file for easier navigation.
index_path=$public_dir/index.html
# Links to add to the index page.
html_links=()

# Wrapper around nbconvert that injects custom reveal.js configuration code into
# the generated HTML file.
function custom_nbconvert()
{
  ipynb_path=$(readlink -f "$1")
  name=${ipynb_path##*/}
  name=${name%.ipynb}.slides.html
  html_dir=$public_dir/notebooks
  html_path=$html_dir/$name

  # Add the link to the list.
  html_links+=("notebooks/$name")

  # Only convert the notebook if it is newer than the output file.
  if [[ $html_path -nt $ipynb_path && $html_path -nt $SELF ]]
  then
    return
  fi
  echo "Converting $ipynb_path to $html_path"

  # https://nbconvert.readthedocs.io/en/latest/config_options.html
  # https://revealjs.com/themes/
  # transitions: none, fade, slide, convex, concave, zoom
  # TagRemovePreprocessor.remove_cell_tags
  # TagRemovePreprocessor.remove_input_tags
  # TagRemovePreprocessor.remove_single_output_tags
  # TagRemovePreprocessor.remove_all_outputs_tags
  jupyter nbconvert \
    -y \
    --log-level=WARN \
    --output-dir="$html_dir" \
    --to slides \
    --SlidesExporter.reveal_theme=white \
    --SlidesExporter.reveal_transition=slide \
    --SlidesExporter.reveal_number='h.v' \
    --TagRemovePreprocessor.remove_input_tags 'hide_input' \
    --TagRemovePreprocessor.remove_all_outputs_tags 'hide_output' \
    "$ipynb"


  # Add custom Reveal configuration settings. There seems to be no way to do this
  # with nbexport at the moment. Previous methods are deprecated.
  # https://revealjs.com/config/
  lines_from_bottom=5
  cat <(head "-n-$lines_from_bottom" "$html_path") <(cat << CONFIG

        const aspect_ratio = 16 / 10;
        const width = 1100;
        Reveal.configure({
          progress: true,
          hash: true,
          width: width,
          height: width / aspect_ratio
        });
CONFIG
  ) <(tail "-n$lines_from_bottom" "$html_path") >> "$tmp_path"
  mv "$tmp_path" "$html_path"
}

# Convert all of the notebooks.
for ipynb in notebooks/*.ipynb
do
  custom_nbconvert "$ipynb"
done

# Generate index page
cat > "$tmp_path" < html/index_head.html
for html_link in "${html_links[@]}"
do
  link_name=${html_link##*/}
  link_name=${link_name%%.*}
  echo "        <li><a href="$html_link">$link_name</a></li>" >> "$tmp_path"
done
cat >> "$tmp_path" < html/index_foot.html
if ! cmp -s "$tmp_path" "$index_path"
then
  echo "Updating $index_path"
  mv -- "$tmp_path" "$index_path"
fi
