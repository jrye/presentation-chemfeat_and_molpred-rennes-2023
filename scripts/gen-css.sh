#!/usr/bin/env bash
set -euo pipefail

# Jupyter notebook seems to strip out inline styles that can change the page
# layout, including "flex-basis". Generate classes with increments of 5% to work
# around this limitation.

if [[ ${BASH_VERSINFO[0]} -lt 5 ]]
then
  echo "warning: You are running Bash version $BASH_VERSION but this script expects at least version 5.0" >&2
fi

SELF=$(readlink -f "${BASH_SOURCE[0]}")
cd "${SELF%/*/*}/css"

rm -f ./sizes.css

# Generate flexbox column size classes.
for pct in {5..95..5}
do
  cat >> ./sizes.css << CSS
.fb${pct} {
  flex-basis: ${pct}%;
}
CSS
done

# Generate reveal.js code height classes.
for height in {10..30}
do
  cat >> ./sizes.css << CSS
.reveal .ch${height} pre code {
  max-height: calc($height * var(--jp-code-font-size)) !important;
}
CSS
done
