#!/usr/bin/env bash
set -euo pipefail

if [[ ${BASH_VERSINFO[0]} -lt 5 ]]
then
  echo "warning: You are running Bash version $BASH_VERSION but this script expects at least version 5.0" >&2
fi

SELF=$(readlink -f "${BASH_SOURCE[0]}")
cd "${SELF%/*/*}"
rm -frv ./public
